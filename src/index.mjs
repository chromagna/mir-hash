import crypto from "crypto";

export const HashGenerator = (opts) => {
    const {
        algorithm,
        supportedAlgorithms,
        saltLength,
    } = opts;

    return {
        verify: function (password, hashString) {
            const {algorithm, salt} = this.parse(hashString);
            if (!supportedAlgorithms.includes(algorithm)) throw new Error(`Invalid algorithm type. Got ${algorithm}. Expected one of ${supportedAlgorithms}.`)
            const newHashString = this.hash(password, algorithm, salt);
            return newHashString === hashString;
        },
        create: function (password) {
            const salt = crypto.randomBytes(saltLength).toString("base64");
            return this.hash(password, algorithm, salt);
        },
        hash: function (password, algorithm, salt) {
            const hash = crypto.createHash(algorithm);
            hash.update(password);
            hash.update(salt, "utf8");
            const digest = hash.digest("base64");
            return `${algorithm}:${salt}:${digest}`;
        },
        parse: function (hashString) {
            const parts = hashString.split(":");
            if (parts.length !== 3) throw new Error(`Invalid hash string. Expected 3 parts. Got ${parts.length}.`);
            const [algorithm, salt, digest] = parts;
            return {algorithm, salt, digest};
        },
    };
};

export const hash = HashGenerator({
    algorithm: "sha256",
    supportedAlgorithms: ["sha256", "sha512"],
    saltLength: 64,
});
