# hash

A very simple string hashing library that exposes two functions:

- HashGenerator
- hash

Use `HashGenerator` if you need to define a hasher that's different from the
default `hash` hasher.

This is how the default `hash` is created using the `HashGenerator`:

```javascript
const myHasher = HashGenerator({
    algorithm: "sha256",
    supportedAlgorithms: ["sha256", "sha512"],
    saltLength: 64,
});
```

## Usage

### Creating

```javascript
const hashString = hash.create("my-secret");
```

### Verifying

```javascript
const verified = hash.verify("my-secret", hashString);
```
